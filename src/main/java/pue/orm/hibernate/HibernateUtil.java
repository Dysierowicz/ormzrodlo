package pue.orm.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static SessionFactory sessionFactory;
	private static Session session;
	
	static {
		sessionFactory = new Configuration().configure().buildSessionFactory();
	}
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public static Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public static void shutdown() {
		getCurrentSession().close();
		getSessionFactory().close();
	}
	
	public static void begin() {
		getCurrentSession().beginTransaction();
	}
	
	public static void commit() {
		getCurrentSession().getTransaction().commit();
	}
	
	public static void rollback() {
		getCurrentSession().getTransaction().rollback();
	}
	
}
