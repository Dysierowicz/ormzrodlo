package pue.orm.main;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pue.orm.hibernate.HibernateUtil;
import pue.orm.model.book.Author;
import pue.orm.model.book.Book;
import pue.orm.model.book.Publisher;
import pue.orm.service.book.AuthorService;
import pue.orm.service.book.BookService;
import pue.orm.service.book.PublisherService;

public class BookStoreApp {

	public static void main(String[] args) {
		BookStoreApp app = new BookStoreApp();
		
		app.insertData();
		
		app.displayData();
		
		//app.updateData();
		
		//app.deleteData();
		
		//app.displayData();
		
		app.shutdown();
	}

	private void insertData() {
		AuthorService authorSrv = AuthorService.getAuthorService();
		authorSrv.addAuthor("Joanne", "Rowling");
		authorSrv.addAuthor("Stephen", "King");
		authorSrv.addAuthor("Paulo", "Coelho");
		authorSrv.addAuthor("Dan", "Brown");
		
		PublisherService publisherSrv = PublisherService.getPublisherService();
		Long publisher1 = publisherSrv.addPublisher("HarperTorch", "United States", "New York");
		Long publisher2 = publisherSrv.addPublisher("Bloomsbury Publishing", "United Kingdom", "London");
		Long publisher3 = publisherSrv.addPublisher("Doubleday", "United States", "New York");
		Long publisher4 = publisherSrv.addPublisher("Random House", "United States", "New York");
		Long publisher5 = publisherSrv.addPublisher("Signet Books", "United States", "New York");
		Long publisher6 = publisherSrv.addPublisher("Manning Publications", "United States", "New York");
		
		BookService bookSrv = BookService.getBookService();
		bookSrv.addBook("The Pilgrimage", publisher1, 1987, new BigDecimal(56.0));
		bookSrv.addBook("The Alchemist", publisher1, 1998, new BigDecimal(60.0));
		bookSrv.addBook("Brida", publisher1, 1990, new BigDecimal(54.0));
		
		bookSrv.addBook("Harry Potter I", publisher2, 1997, new BigDecimal(55.0));
		bookSrv.addBook("Harry Potter II", publisher2, 1998, new BigDecimal(80.0));
		bookSrv.addBook("Harry Potter III", publisher2, 1999, new BigDecimal(45.0));
		
		bookSrv.addBook("Carrie", publisher3, 1977, new BigDecimal(58.0));
		bookSrv.addBook("The Shining", publisher3, 1978, new BigDecimal(85.0));
		bookSrv.addBook("The Green Mile", publisher3, 1996, new BigDecimal(65.0));
		
		bookSrv.addBook("The Da Vinci Code", publisher4, 2003, new BigDecimal(45.0));
		bookSrv.addBook("The Lost Symbol", publisher4, 2009, new BigDecimal(62.0));
	}
	
	private void displayData() {
		displayAuthors();
		
		displayBooks();
	}

	private void displayAuthors() {
		AuthorService authorSrv = AuthorService.getAuthorService();

		System.out.println("----- Autorzy -----");
		List authors = authorSrv.getAllAuthors();
		
		for (Iterator iterator = authors.iterator(); iterator.hasNext(); )
		{
			Author author = (Author) iterator.next();
			System.out.println("Imi�: " + author.getFirstName() + 
							", Nazwisko: " + author.getLastName());
		}
	}
	
	private void displayBooks() {
		BookService bookSrv = BookService.getBookService();

		System.out.println("----- Ksi��ki -----");
		List books = bookSrv.getAllBooks();
		
		for (Iterator iterator = books.iterator(); iterator.hasNext(); )
		{
			Book book = (Book) iterator.next();
			
			System.out.println(" ---> Id : " + book.getId());
			
			System.out.println("Tytuł: " + book.getTitle() + 
							", rok wydania: " + book.getPublishingYear());
			
			Publisher publisher = book.getPublisher();
			if ( publisher != null ) {
				System.out.println("Wydawnictwo : " + publisher.getName());
			}
			else {
				System.out.println("Wydawnictwo : nieznane");
			}
		}
	}
	
	private void updateData() {
		AuthorService authorSrv = AuthorService.getAuthorService();
		authorSrv.updateAuthor(Long.valueOf(3), "John", "Grisham");
	}
	
	private void deleteData() {
		AuthorService authorSrv = AuthorService.getAuthorService();
		authorSrv.deleteAuthor(Long.valueOf(2));
	}
	
	private void shutdown() {
		HibernateUtil.shutdown();
	}

	/*
	public static void main(String[] args) {
		SessionFactory factory = HibernateUtil.getSessionFactory();

		Session session = factory.getCurrentSession();
		
		System.out.println("Hello Hibernate!");
		
		session.close();
		HibernateUtil.shutdown();
	}
	*/
}
