package pue.orm.service.book;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import pue.orm.hibernate.HibernateUtil;
import pue.orm.model.book.Author;

public class AuthorService {
	private static AuthorService authorSrv;
	
	static {
		authorSrv = new AuthorService();
	}
	
	public static AuthorService getAuthorService() {
		return authorSrv;
	}
	
	/* CREATE */
	public Long addAuthor(String firstName, String lastName) {
		Author author = new Author();
		author.setFirstName(firstName);
		author.setLastName(lastName);
		
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		Long authorId = (Long) session.save(author);
		
		HibernateUtil.commit();
		
		
		return authorId;
	}
	
	/* READ */
	public Author getAuthor(Long authorId) {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		Author author = (Author)session.get(Author.class, authorId);
		
		HibernateUtil.commit();
		
		return author;
	}
	
	public Author getAuthorByName(String firstName, String lastName) {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();

		Query query = session.createQuery(
				"FROM Author WHERE firstName = :firstName "
			  + "AND lastName = :lastName");
		
		query.setParameter("firstName", firstName);
		query.setParameter("lastName", lastName);
		
		Author author = (Author) query.uniqueResult();
		
		HibernateUtil.commit();
		
		return author;
	}
	
	public List<Author> getAllAuthors() {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		List authors = session.createQuery("FROM Author").list();
		
		HibernateUtil.commit();
		
		return authors;
	}
	
	/* UPDATE */
	public void updateAuthor(Long authorId, String firstName, String lastName) {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		Author author = (Author)session.get(Author.class, authorId);
		author.setFirstName(firstName);
		author.setLastName(lastName);
		
		session.update(author);
		
		HibernateUtil.commit();
		
		
	}
	
	/* DELETE */
	public void deleteAuthor(Long authorId) {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		Author author = (Author)session.get(Author.class, authorId);
		
		session.delete(author);
				
		HibernateUtil.commit();
		
		
	}
}
