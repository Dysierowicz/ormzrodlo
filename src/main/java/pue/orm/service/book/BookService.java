package pue.orm.service.book;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import pue.orm.hibernate.HibernateUtil;
import pue.orm.model.book.Author;
import pue.orm.model.book.Book;
import pue.orm.model.book.Publisher;

public class BookService {
	private static BookService bookSrv;
	
	static {
		bookSrv = new BookService();
	}
	
	public static BookService getBookService() {
		return bookSrv;
	}
	
	/* CREATE */
	public Long addBook(String title, Long publisherId, Integer publishingYear, 
			            BigDecimal price) {
		Book book = new Book();
		book.setTitle(title);
		book.setPublishingYear(publishingYear);
		book.setPrice(price);
		
		Publisher publisher = PublisherService.getPublisherService().getPublisher(publisherId);
		if ( publisher != null ) {
			book.setPublisher(publisher);
		}
		
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		Long bookId = (Long) session.save(book);
		
		HibernateUtil.commit();
		
		return bookId;
	}
	
	/* READ */
	public Book getBook(Long bookId) {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		Book book = (Book)session.get(Book.class, bookId);
		
		HibernateUtil.commit();
		return book;
	}
	
	public List<Book> getAllBooks() {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		List books = session.createQuery("FROM Book").list();
		
		HibernateUtil.commit();
		
		return books;
	}
	
	public List<Book> getBooksByYear(Integer beginYear, Integer endYear) {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();

		Query query = session.createQuery(
				"FROM Book WHERE publicationYear BETWEEN :beginYear "
			  + "AND :endYear");
		
		query.setParameter("beginYear", beginYear);
		query.setParameter("endYear", endYear);
		
		List books = query.list();
		
		HibernateUtil.commit();
		
		return books;
	}
}

