package pue.orm.service.book;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import pue.orm.hibernate.HibernateUtil;
import pue.orm.model.book.Author;
import pue.orm.model.book.Publisher;

public class PublisherService {
	private static PublisherService publisherSrv;
	
	static {
		publisherSrv = new PublisherService();
	}
	
	public static PublisherService getPublisherService() {
		return publisherSrv;
	}
	
	/* CREATE */
	public Long addPublisher(String name, String country, String city) {
		Publisher publisher = new Publisher();
		publisher.setName(name);
		publisher.setCountry(country);
		publisher.setCity(city);
		
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		Long publisherId = (Long) session.save(publisher);
		
		HibernateUtil.commit();
		
		
		return publisherId;
	}
	
	/* READ */
	public Publisher getPublisher(Long publisherId) {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		Publisher publisher = (Publisher)session.get(Publisher.class, publisherId);
		
		HibernateUtil.commit();

		return publisher;
	}
	
	public List<Publisher> getAllPublishers() {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		List publishers = session.createQuery("FROM Publisher").list();
		
		HibernateUtil.commit();
		
		return publishers;
	}
	
	/* UPDATE */
	public void updatePublisher(Long publisherId, String name, 
			                    String country, String city) {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		Publisher publisher = (Publisher)session.get(Publisher.class, publisherId);
		publisher.setName(name);
		publisher.setCountry(country);
		publisher.setCity(city);
		
		session.update(publisher);
		
		HibernateUtil.commit();
		
		
	}
	
	/* DELETE */
	public void deletePublisher(Long publisherId) {
		Session session = HibernateUtil.getCurrentSession();
		HibernateUtil.begin();
		
		Publisher publisher = (Publisher)session.get(Publisher.class, publisherId);
		
		session.delete(publisher);
				
		HibernateUtil.commit();
		
		
	}

}
